"""Tropospheric propagation models.

This implements corrections for refractive bending and propagation delay in
the electrically neutral atmosphere (mostly the troposphere and stratosphere).
"""
