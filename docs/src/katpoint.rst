katpoint package
================

Submodules
----------

katpoint.antenna module
-----------------------

.. automodule:: katpoint.antenna
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.body module
--------------------

.. automodule:: katpoint.body
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.catalogue module
-------------------------

.. automodule:: katpoint.catalogue
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.conversion module
--------------------------

.. automodule:: katpoint.conversion
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.delay\_correction module
---------------------------------

.. automodule:: katpoint.delay_correction
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.delay\_model module
----------------------------

.. automodule:: katpoint.delay_model
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.flux module
--------------------

.. automodule:: katpoint.flux
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.model module
---------------------

.. automodule:: katpoint.model
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.pointing module
------------------------

.. automodule:: katpoint.pointing
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.projection module
--------------------------

.. automodule:: katpoint.projection
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.target module
----------------------

.. automodule:: katpoint.target
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.timestamp module
-------------------------

.. automodule:: katpoint.timestamp
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.troposphere.delay module
---------------------------------

.. automodule:: katpoint.troposphere.delay
   :members:
   :undoc-members:
   :show-inheritance:

katpoint.troposphere.refraction module
--------------------------------------

.. automodule:: katpoint.troposphere.refraction
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: katpoint
   :members:
   :undoc-members:
   :show-inheritance:
